var express = require('express');
const productHelpers = require('../helpers/product-helpers');
var router = express.Router();
var db = require("../config/connection ")
var collection = require("../config/collections")

/* GET users listing. */
router.get('/', function (req, res, next) {
  productHelpers.getAllProducts().then((products) => {
    res.render('admin/view-products', { admin: true, products })
  })
});
router.get("/add-product", (req, res) => {
  res.render('admin/add-product', { admin: true })
})
router.get("/add-variant", (req, res) => {
  res.render('admin/add-variant', { admin: true })
})

router.post("/add-variant", async (req, res) => {

  const variantDoc = {
    product: req.body.product,
    size: req.body.size,
    price: req.body.price,
  };

  const product = await db.get().collection(collection.PRODUCT_COLLECTION).findOne({ _id: req.body.product });



  try {
    const variant = await db.get().collection(collection.VARIANT_COLLECTION).insertOne(variantDoc);
    product.variants = variant.ops;
    res.status(200).json({ message: 'Successfully added variant', product });
  } catch (ex) {
    console.log('here?');
    res.status(400).json({ message: 'Failed to add variant. Please try again' });
  }

})

router.post('/add-product', (req, res) => {
  console.log(req.body);
  console.log(req.files.Image);

  productHelpers.addProduct(req.body, (id) => {
    let image = req.files.Image
    image.mv("./public/product-images/" + id + ".png", (err) => {
      if (!err) {
        res.render("admin/add-product")
      } else {
        console.log(err);
      }
    })
  })
})
module.exports = router;
