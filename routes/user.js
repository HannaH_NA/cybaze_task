var express = require('express');
const { ObjectId } = require('mongodb');
var router = express.Router();
const productHelpers = require('../helpers/product-helpers');
const userHelpers = require('../helpers/user-helpers')
/* GET users listing. */
router.get('/', function (req, res, next) {
  productHelpers.getAllProducts().then((products) => {
    console.log(products);
    res.render('user/view-products', { products });

  })
});
router.get('/cart', async (req, res) => {
  let products = await userHelpers.getCartProducts(1)
  console.log(products);
  res.render('user/cart')
})
router.get('/add-to-cart/:id', (req, res) => {
  userHelpers.addToCart(req.params.id, 1).then(() => {
    res.redirect('/')
  })
})

module.exports = router;
